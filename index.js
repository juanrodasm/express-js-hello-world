'use strict'

const express = require('express')

const app = express()

app.get('/', (req, res) => {
    res.send('Hello World')
})

app.get('/contacto', (req, res) => {
    res.send('Contacto')
})

app.listen(3000, () => {
    console.log("Iniciando servidor")
})